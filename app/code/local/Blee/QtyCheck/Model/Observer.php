<?php
class Blee_QtyCheck_Model_Observer
{
	public function updateListener(Varien_Event_Observer $observer)
	{
		if (Mage::getStoreConfig('checkout/cart/qty_reduce_enable') == false) {
			return;
		}

		$request     = $observer->getEvent()->getControllerAction()->getRequest();
		$updateData  = $request->getParam('cart');
		$quote       = Mage::getSingleton('checkout/cart')->getQuote();
		$showMessage = false;

		// Setup data maps
		$skuQtyMap  = array();
		$itemSkuMap = array();
		foreach ($quote->getAllItems() as $item) {
			$sku       = $item->getProduct()->getSku();
			$stockItem = Mage::getModel('cataloginventory/stock_item')
				->loadByProduct($item->getProduct()->getId());

			$itemSkuMap[$item->getId()] = $sku;

			if (empty($skuQtyMap[$sku])) {
				$skuQtyMap[$sku] = 0;
			}
			$skuQtyMap[$sku] = $stockItem->getQty();
		}

		// Check qty
		foreach ($updateData as $itemId => $data) {
			$qty = (int)$data['qty'];
			if ($qty < 0) {
				$qty = 0;
			}

			$sku = $itemSkuMap[$itemId];
			if ($skuQtyMap[$sku] >= $qty) {
				$skuQtyMap[$sku] -= $qty;
			} else if ($skuQtyMap[$sku] > 0) {
				$qty             = $skuQtyMap[$sku];
				$skuQtyMap[$sku] = 0;
				$showMessage     = true;
			} else {
				$qty         = 0;
				$showMessage = true;
			}

			$updateData[$itemId]['qty'] = (string)$qty;
		}

		// Update
		$request->setParam('cart', $updateData);

		if ($showMessage) {
			$message = 'Our apologies. The quantity you requested is not '
			.'available at this time. We have added the available '
			.'quantities to your cart.';

			if (Mage::getStoreConfig('checkout/cart/qty_reduce_message_cart_update')) {
				$message = Mage::getStoreConfig('checkout/cart/qty_reduce_message_cart_update');
			}
			$message = Mage::helper('checkout')->__($message);

			Mage::getSingleton('core/session')->addError($message);
		}
	}

	public function addListener(Varien_Event_Observer $observer)
	{
		if (Mage::getStoreConfig('checkout/cart/qty_reduce_enable') == false) {
			return;
		}

		$custSkuQtyMap = array();
		$data          = Mage::getSingleton('customer/session')->getRawCart();
		if (is_array($data)) {
			foreach ($data as $sku => $skuMap) {
				if (isset($custSkuQtyMap[$sku]) === false) {
					$custSkuQtyMap[$sku] = 0;
				}
				foreach ($skuMap as $skuData) {
					$custSkuQtyMap[$sku] += $skuData['qty'];
				}
			}
		}

		$request = $observer->getEvent()->getControllerAction()->getRequest();

		if ($request->getParam('product') && $request->getParam('qty')) {
			$productId = $request->getParam('product');
			$addQty    = $request->getParam('qty');

			$product = Mage::getModel('catalog/product')
				->setStoreId(Mage::app()->getStore()->getId())
				->load($productId);
			if ($product->getId()) {

				$cartQty = 0;
				if (isset($custSkuQtyMap[$product->getSku()])) {
					$cartQty = $custSkuQtyMap[$product->getSku()];
				}

				$stockItem = Mage::getModel('cataloginventory/stock_item')
					->loadByProduct($product->getId());

				if (
					$stockItem
					&& $stockItem->getQty() > 0
					&& $stockItem->getQty() < $addQty
				) {
					if ($stockItem->getQty() - $cartQty) {
						$request->setParam('qty', $stockItem->getQty() - $cartQty);
						$cartQty += $stockItem->getQty() - $cartQty;
					} else {
						// Don't have enough stock to add anymore, prevent add
						$request->setParam('product', '-');
					}

					$message = 'Our apologies. The quantity you requested is not '
					.'available at this time. We have added the available '
					.'quantity of %d to your cart.';

					if (Mage::getStoreConfig('checkout/cart/qty_reduce_message_add')) {
						$message = Mage::getStoreConfig('checkout/cart/qty_reduce_message_add');
					}

					$message = Mage::helper('checkout')->__($message, $cartQty);

					Mage::getSingleton('core/session')->addError($message);
				}
			}
		}
	}
}
