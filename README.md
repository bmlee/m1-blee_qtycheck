# README #

When more than available product qty is added to cart, it will modify the qty to what's available in stock rather than just throwing an error.

### Magento events used ###
* controller_action_predispatch_checkout_cart_add
* controller_action_predispatch_checkout_cart_updatePost